'use strict'
const AWS = require('aws-sdk')
const sqs = new AWS.SQS({ region: 'us-east-1' })

module.exports.hello = async (event) => {
  await sqs
    .sendMessage({
      MessageAttributes: {
        Title: {
          DataType: 'String',
          StringValue: `Try Serverless`,
        },
      },
      MessageBody: `${event}`,
      QueueUrl: '<YOUR QUEUE URL>',
    })
    .promise()

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'You successfully called the function.',
        input: event,
      },
      null,
      2
    ),
  }
}
